//
//  MainViewController.swift
//  Yelp
//
//  Created by Nicholas Chung on 2/7/17.
//  Copyright © 2017 Nicholas Chung. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var businessMap: MKMapView!
    @IBOutlet weak var mapContainerView: UIView!
    
    var businessess: [Business?] = []
    var filteredBusinesses: [Business?] = []
    
    var isMoreDataLoading = false
    var loadingMoreView:InfiniteScrollActivityView?
    
    var locationManager = CLLocationManager()
    var searchController: UISearchController!
    
    var tMFlag = 0
    var searchText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            self.navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 211/255, green: 35/255, blue: 35/255, alpha: 1/1)
        } else {
            // Fallback on earlier versions
        }
        
        businessMap.delegate = self
        
        filterButton.sizeToFit()
        mapButton.sizeToFit()
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        self.searchController.searchBar.setShowsCancelButton(false, animated: false)
        searchController.searchBar.placeholder = "e.g. tacos, delivery, Max's"
        self.navigationItem.titleView = searchController.searchBar
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 92
        tableView.rowHeight = UITableViewAutomaticDimension
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 200
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        let frame = CGRect(x: 0, y: tableView.contentSize.height, width: tableView.bounds.size.width, height: InfiniteScrollActivityView.defaultHeight)
        loadingMoreView = InfiniteScrollActivityView(frame: frame)
        loadingMoreView!.isHidden = true
        tableView.addSubview(loadingMoreView!)
        
        var insets = tableView.contentInset
        insets.bottom += InfiniteScrollActivityView.defaultHeight
        tableView.contentInset = insets
    }
    
    func loadBusinesses() {
        let _ = YelpClient.sharedInstance.searchWithTerm("", offset: nil, latitude: locationManager.location?.coordinate.latitude, longitude: locationManager.location?.coordinate.longitude, sort: .distance, categories: nil, deals: nil) { (businessess, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.businessess = businessess!
                self.filteredBusinesses = businessess!
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (!isMoreDataLoading) {
            // Calculate the position of one screen length before the bottom of the results
            let scrollViewContentHeight = tableView.contentSize.height
            let scrollOffsetThreshold = scrollViewContentHeight - tableView.bounds.size.height
            
            // When the user has scrolled past the threshold, start requesting
            if(scrollView.contentOffset.y > scrollOffsetThreshold && tableView.isDragging) {
                isMoreDataLoading = true
                
                // Update position of loadingMoreView, and start loading indicator
                let frame = CGRect(x: 0, y: tableView.contentSize.height, width: tableView.bounds.size.width, height: InfiniteScrollActivityView.defaultHeight)
                loadingMoreView?.frame = frame
                loadingMoreView!.startAnimating()
                
                // Code to load more results
                loadMoreData()		
            }
        }
    }
    
    func loadMoreData() {
        let _ = YelpClient.sharedInstance.searchWithTerm(searchText, offset: filteredBusinesses.count, latitude: locationManager.location?.coordinate.latitude, longitude: locationManager.location?.coordinate.longitude, sort: .distance, categories: nil, deals: nil) { (businessess, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            self.businessess = self.businessess + businessess!
            self.filteredBusinesses = self.filteredBusinesses + businessess!
            self.businessMap.removeAnnotations(self.businessMap.annotations)
            self.tableView.reloadData()
            self.isMoreDataLoading = false
        }
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.setShowsCancelButton(false, animated: false)
        filterButton.titleLabel?.text = "Cancel"
        filterButton.sizeToFit()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        filterButton.titleLabel?.text = "Filter"
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        guard let searchText = searchController.searchBar.text else {
            return
        }
        let _ = YelpClient.sharedInstance.searchWithTerm(searchText, offset: nil, latitude: locationManager.location?.coordinate.latitude, longitude: locationManager.location?.coordinate.longitude, sort: .distance, categories: nil, deals: nil) { (businessess, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            self.businessess = businessess!
            self.filteredBusinesses = businessess!
            self.businessMap.removeAnnotations(self.businessMap.annotations)
            self.tableView.reloadData()
        }
        
        
    }
    
    @IBAction func onFilter(_ sender: Any) {
        if filterButton.titleLabel?.text == "Filter" {
            
        } else {
            searchController.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onMap(_ sender: Any) {
        if mapButton.titleLabel?.text == "Map" {
            UIView.transition(with: mapContainerView, duration: 1.0, options: .transitionFlipFromRight, animations: {
                self.tableView.isHidden = true
                self.businessMap.isHidden = false
            }, completion: { (result) in
                self.mapButton.titleLabel?.text = "List"
            })
        } else if mapButton.titleLabel?.text == "List" {
            UIView.transition(with: mapContainerView, duration: 1.0, options: .transitionFlipFromRight, animations: {
                
                self.businessMap.isHidden = true
                self.tableView.isHidden = false
            }, completion: { (result) in
                self.mapButton.titleLabel?.text = "Map"
            })
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredBusinesses.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessCell", for: indexPath) as! BusinessCell
        
        guard let business = filteredBusinesses[indexPath.row] else {
            return cell
        }
        
        if let name = business.name {
            cell.nameLabel.text = "\(indexPath.row + 1). \(name)"
        }
        if let address = business.address {
            cell.addressLabel.text = address
        }
        if let distance = business.distance {
            cell.distanceLabel.text = distance
        }
        if let type = business.categories {
            cell.typeLabel.text = type
        }
        if let reviews = business.reviewCount {
            cell.reviewLabel.text = "\(reviews) Reviews"
        }
        
        guard let ratingImageURL = business.ratingImageURL else {
            return cell
        }
        cell.reviewView.setImageWith(ratingImageURL)
        
        guard let imageURL = business.imageURL else {
            return cell
        }
        cell.businessView.setImageWith(imageURL)
        cell.businessView.layer.cornerRadius = 5
        cell.businessView.clipsToBounds = true
        
        if let latitude = business.latitude {
            let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: business.longitude!)
            let annotation = BusinesssAnnotation(title: business.name!, coordinate: coordinates, business: business, index: indexPath.row)
            businessMap.addAnnotation(annotation)
        }
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        let cell = sender as! BusinessCell
        let vc = segue.destination as! DetailViewController
        let indexPath = tableView.indexPath(for: cell)
        vc.business = businessess[(indexPath?.row)!]
        vc.ratingView = cell.reviewView
     }
    
    
}

extension MainViewController: MKMapViewDelegate, CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let span = MKCoordinateSpanMake(0.025, 0.025)
            let region = MKCoordinateRegionMake(location.coordinate, span)
            businessMap.setRegion(region, animated: true)
            businessMap.showsUserLocation = true
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: BusinesssAnnotation.classForCoder()) {
            guard let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "BusinessAnnotation") else {
                let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "BusinessAnnotation")
                
                let annot = annotation as! BusinesssAnnotation
                annotationView.isEnabled = true
                annotationView.canShowCallout = true
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: annot.business.imageURL!, options: SDWebImageDownloaderOptions.continueInBackground, progress: nil, completed: { (image, data, error, result) in
                    if let image = image {
                        let imageView = UIImageView(image: image)
                        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                        imageView.layer.cornerRadius = imageView.layer.frame.size.width / 2
                        imageView.layer.masksToBounds = true
//                        imageView.layer.borderColor = UIColor.black.cgColor
//                        imageView.layer.borderWidth = 2.5
                        annotationView.addSubview(imageView)
                        annotationView.frame = imageView.frame
                    } else if let error = error {
                        print(error.localizedDescription)
                    }
                })
                return annotationView
            }
            annotationView.canShowCallout = true
            return annotationView
        }
        
        return nil
    }
}
